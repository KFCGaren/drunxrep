﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DrunxMain : MonoBehaviour {

    public Text ime1, ime2, akcija, challenges;
    public List<string> D, C = new List<string>();
    public List<string> akcije;
    public string[] akcijeStandard = { "Kiss in the cheek" , "Cheek touch" , "Nose touch" , "Hug" , "Ask the girl something" , "Ask the boy something" };
    public List<InputField> DJ, CJ = new List<InputField>();
    public InputField chlIF;
    System.Random rnd = new System.Random();
    public GameObject canv1, canv2, rand1, rand2, next, panel;
    bool stand = true;
    GameObject chlIFObj;

    public void Start()
    {
        chlIFObj = chlIF.gameObject;
    }

    public void Done()
    {
        foreach (InputField IF in DJ)
        {
            if (IF.text != "" && IF.text != " ")
            {
                D.Add(IF.text);
            }
        }
        foreach (InputField IF in CJ)
        {
            if (IF.text != "" && IF.text != " ")
            {
                C.Add(IF.text);
            }
        }
        canv1.SetActive(false);
        canv2.SetActive(true);
    }

    public void Challenges()
    {
        panel.SetActive(!panel.activeInHierarchy);
        stand = !stand;
        UseStandard();
    }

    public void UseStandard()
    {
        challenges.text = "";
        if (stand)
        {
            chlIFObj.SetActive(true);
            foreach (string ch in akcije)
            {
                challenges.text += ch + "\n";
            }
            stand = !stand;
        }
        else if (!stand)
        {
            chlIFObj.SetActive(false);
            foreach (string ch in akcijeStandard)
            {
                challenges.text += ch + "\n";
            }
            stand = !stand;
        }
    }


    public void Next()
    {
        rand2.SetActive(false);
        rand1.SetActive(true);
        next.SetActive(false);
        ime1.text = ime2.text = akcija.text = "";
    }

    public void Rand()
    {
        if (rand1.activeInHierarchy)
        {
            ime1.text = D[rnd.Next(D.Count)];
            ime2.text = C[rnd.Next(C.Count)];
            rand1.SetActive(false);
            rand2.SetActive(true);
            StartCoroutine(FadeTextToFullAlpha(1f, ime1));
            StartCoroutine(FadeTextToFullAlpha(1f, ime2));
        }
        else
        {
            if (stand)
            {
                akcija.text = akcijeStandard[rnd.Next(akcijeStandard.Length)];
            } else
            {
                akcija.text = akcije[rnd.Next(akcije.Count)];
            }
            StartCoroutine(FadeTextToFullAlpha(1f, akcija));
            next.SetActive(true);
        }

    }

    public void ChlIFChange()
    {
        akcije.Add(chlIF.text);
        challenges.text += chlIF.text + "\n";
        chlIF.text = "";
    }

    public void Back()
    {
        ime1.text = ime2.text = akcija.text = "";
        rand1.SetActive(true);
        rand2.SetActive(false);
        canv2.SetActive(false);
        canv1.SetActive(true);
    }

    public IEnumerator FadeTextToFullAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
        while (i.color.a < 1.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t)/2.5f);
            yield return null;
        }
    }
}
